from numpy import *
from numpy import linalg as LA
import numpy.random
from sklearn.datasets import fetch_mldata
import sklearn.preprocessing
from numpy import *
import matplotlib
# matplotlib.use("agg")
import matplotlib.pyplot as plt
import sys

class NNAlgo:

    def __init__(self):
        '''
        Initialize data set
        '''

        mnist = fetch_mldata('MNIST original')
        data = mnist['data']
        labels = mnist['target']
        idx = numpy.random.RandomState(0).choice(70000, 11000)
        self.train = data[idx[:10000], :].astype(int)
        self.train_labels = labels[idx[:10000]]
        self.test = data[idx[10000:], :].astype(int)
        self.test_labels = labels[idx[10000:]]

    def _knn(self,n,k,query):
        '''
        Perform KNN algorithm for a given query image and N,K values
        :param n: Number of training images to use
        :param k: Number of neighbors to keep track of
        :param query: The query image
        :return: The best label for the given query
        '''

        nn = []
        for i in range(n):
            l2 = self._calculate_l2(i, query)
            if (len(nn) < k):
                nn += [(i, l2)]
            else:
                sorted(nn, key=lambda tup: tup[1])
                for j in range(k):
                    if (l2 < nn[j][1]):
                        nn = nn[:j] + [(i, l2)] + nn[j:k - 1]
                        break
        lbls = [0 for i in range(10)]
        for neighbor in nn:
            lbls[int(self.train_labels[neighbor[0]])] += 1
        max = lbls[0]
        lbl = 0
        for l in range(10):
            if (lbls[l] > max):
                max = lbls[l]
                lbl = l
        return lbl

    def _calculate_errors(self,n,k):
        '''
        Calculate the percentage of errors for a given set of guesses
        :param n: The number of training images to use
        :param k: The number of neighbors to pass onto KNN algorithm
        :return: Percentage of errors
        '''

        return numpy.mean([self._knn(n,k,query) for query in self.test] != self.test_labels)

    def _calculate_l2(self,i,query):
        '''
        Calculate the L2-Euclidean distance between two images
        :param i: The index of training image to compare the query against
        :param query: The query images
        :return: The L2-Euclidean distance
        '''

        return LA.norm(self.train[i] - query)

    def Qb(self):
        return self._calculate_errors(1000, 10)

    def Qc(self):
        Ks = [i for i in range(1, 101)]
        errors = [self._calculate_errors(1000,k) for k in Ks]
        plt.plot(Ks, errors, "o")
        plt.show()

# MAIN #
algo = NNAlgo()
print(algo.Qb())